Mar 2021: compared to "upstream" master branch, the differences are insignificant.

In this sourcetree, the docs have been slightly expanded//corrected, and the init (init.in) might render a toolbar
"Menu" button in the as-shipped default configuration... but that's about it. See debian/changelog for additional remarks.

___________________________

Fluxbox is a windowmanager for X. Its sourcecode is distributed under an MIT license.
Fluxbox was derived from the Blackbox v0.61.1 code.

debian 9 (aka "stretch") repository will contain fluxbox v1.35 (FROM BACK IN 2015!).

Although this custom build, with default menu, keybinds, etc. tailored to antiX, is based on:

http://git.fluxbox.org/fluxbox.git/commit/?h=prep/release-1.4.0

I'm calling it "v1.3.8" so that if/when the official v1.4.0 lands in debian repos, antiX users
can just upgrade to the "debian official" packages.

To build an installable debian package, after downloading this source code, refer to the
instruction within the INSTALL file
